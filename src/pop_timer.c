/*
 * Description: pop timer定时器功能
 * Author: xiasonglee@gmail.com
 * Date: 2023-6-22
 */

#include "pop_timer.h"

#include <unistd.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/timerfd.h>
#include <time.h>
#include "pop_event.h"

struct pop_timer {
    int fd;
    uint32_t interval; /* 单位毫秒 */
    bool loop;
    pop_timer_cb timer_cb;
    void *usrdata;
};

static void calculate_over_time(uint32_t interval, struct itimerspec *ts)
{
    struct timespec start;
    clock_gettime(CLOCK_MONOTONIC, &start);

    ts->it_value.tv_sec = start.tv_sec + interval / 1000;
    uint64_t nsec = start.tv_nsec + (interval % 1000) * 1000000;
    if (nsec >= 1000000000) { /* 如果超出1秒则转换为秒 */
        ts->it_value.tv_sec++;
        nsec -= 1000000000;
    }
    ts->it_value.tv_nsec = nsec;
}

static void timer_proc(pop_event_t *event, uint32_t what, void *usrdata)
{
    if (usrdata == NULL) {
        return;
    }
    struct pop_timer *timer = (struct pop_timer *)usrdata;

    uint64_t exp;
    (void)read(timer->fd, &exp, sizeof(exp));

    if (timer->timer_cb != NULL) {
        timer->timer_cb(timer, timer->usrdata);
    }

    if (timer->loop) {
        struct itimerspec ts = { 0 };
        calculate_over_time(timer->interval, &ts);
        if (timerfd_settime(timer->fd, TFD_TIMER_ABSTIME, &ts, NULL) == -1) {
            printf("Failed to settime timerfd, errno %d.\n", errno);
            close(timer->fd);
            return;
        }
    } else {
        close(timer->fd);
        free(timer);
    }
}

static int create_timerfd(uint32_t millsec)
{
    int fd = timerfd_create(CLOCK_MONOTONIC, TFD_NONBLOCK);
    if (fd < 0) {
        printf("Failed to create timerfd, errno %d.\n", errno);
        return -1;
    }

    struct itimerspec ts = { 0 };
    calculate_over_time(millsec, &ts);
    if (timerfd_settime(fd, TFD_TIMER_ABSTIME, &ts, NULL) == -1) {
        printf("Failed to settime timerfd, errno %d.", errno);
        close(fd);
        return -1;
    }
    return fd;
}

pop_timer_t pop_timer_create(uint32_t millsec, bool loop,
    pop_timer_cb cb, void *usrdata)
{
    struct pop_timer *timer = (struct pop_timer *)malloc(sizeof(*timer));
    if (timer == NULL) {
        return NULL;
    }
    *timer = (struct pop_timer){ 0 };
    timer->interval = millsec;
    timer->loop = loop;
    timer->timer_cb = cb;
    timer->usrdata = usrdata;

    timer->fd = create_timerfd(millsec);
    if (timer->fd < 0) {
        free(timer);
        return NULL;
    }

    pop_event_t *evt = pop_event_create(timer->fd, timer);
    pop_event_add_watch(evt, POP_EVENT_IN, timer_proc);
    return timer;
}
