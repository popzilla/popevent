/*
 * Description: pop event对外接口
 * Author: xiasonglee@gmail.com
 * Date: 2023-3-4
 */

#include "pop_event.h"
#include <unistd.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/epoll.h>
#include <errno.h>

#define MAX_EVENT_NUM 256

struct pop_event_base {
    int epfd;
};

typedef struct pop_event pop_event_t;
struct pop_event {
    int fd;
    pop_event_cb_t cb;
    void *usrdata;
};

static __thread struct pop_event_base g_event_base = {
    .epfd = -1
};

static uint32_t pop_what_to_epoll_events(uint32_t what)
{
    uint32_t events = 0;
    if (what & POP_EVENT_IN) {
        events |= EPOLLIN;
    }

    if (what & POP_EVENT_OUT) {
        events |= EPOLLOUT;
    }

    if (what & POP_EVENT_HUP) {
        events |= EPOLLHUP;
    }

    if (what & POP_EVENT_RDHUP) {
        events |= EPOLLRDHUP;
    }

    if (what & POP_EVENT_ERR) {
        events |= EPOLLERR;
    }

    if (what & POP_EVENT_ET) {
        events |= EPOLLET;
    }
    return events;
}

static uint32_t epoll_events_to_pop_what(uint32_t events)
{
    uint32_t what = 0;
    if (events & EPOLLIN) {
        what |= POP_EVENT_IN;
    }

    if (events & EPOLLOUT) {
        what |= POP_EVENT_OUT;
    }

    if (events & EPOLLHUP) {
        what |= POP_EVENT_HUP;
    }

    if (events & EPOLLRDHUP) {
        what |= POP_EVENT_RDHUP;
    }

    if (events & EPOLLERR) {
        what |= POP_EVENT_ERR;
    }
    return what;
}

pop_event_t *pop_event_create(int fd, void *usrdata)
{
    pop_event_t *evt = (pop_event_t *)malloc(sizeof(*evt));
    if (evt == NULL) {
        return NULL;
    }
    *evt = (pop_event_t) { 0 };
    evt->fd = fd;
    evt->usrdata = usrdata;
    return evt;
}

int pop_event_add_watch(pop_event_t *event, uint32_t what, pop_event_cb_t cb)
{
    if (event == NULL) {
        return -1;
    }
    event->cb = cb;

    struct epoll_event ep_evt = { 0 };
    ep_evt.events = pop_what_to_epoll_events(what);
    ep_evt.data.ptr = event;
    if (epoll_ctl(g_event_base.epfd, EPOLL_CTL_ADD, event->fd, &ep_evt) != 0) {
        printf("Failed to add event to epoll, errno %d.", errno);
        return -1;
    }
    return 0;
}

int pop_event_mod_watch(pop_event_t *event, uint32_t what, pop_event_cb_t cb)
{
    if (event == NULL) {
        return -1;
    }
    event->cb = cb;

    struct epoll_event ep_evt = { 0 };
    ep_evt.events = pop_what_to_epoll_events(what);
    ep_evt.data.ptr = event;
    if (epoll_ctl(g_event_base.epfd, EPOLL_CTL_MOD, event->fd, &ep_evt) != 0) {
        printf("Failed to mod event to epoll, errno %d.", errno);
        return -1;
    }
    return 0;
}

int pop_event_del_watch(pop_event_t *event)
{
    if (event == NULL) {
        return -1;
    }

    struct epoll_event ep_evt = { 0 };
    if (epoll_ctl(g_event_base.epfd, EPOLL_CTL_DEL, event->fd, &ep_evt) != 0) {
        printf("Failed to del event to epoll, errno %d.", errno);
        return -1;
    }
    return 0;
}

void pop_event_destroy(pop_event_t *event)
{
    if (event != NULL) {
        free(event);
    }
}

static void pop_event_proc(pop_event_t *event, uint32_t what)
{
    if ((event == NULL) || (event->cb == NULL)) {
        return;
    }
    event->cb(event, what, event->usrdata);
}

void pop_event_loop(void)
{
    while (true) {
        struct epoll_event events[MAX_EVENT_NUM];
        int num = epoll_wait(g_event_base.epfd, events, MAX_EVENT_NUM, -1);
        if (num <= 0) {
            printf("Epoll wait return -1, errno %d.", errno);
            continue;
        }

        for (int i = 0; i < num; i++) {
            pop_event_t *event = events[i].data.ptr;
            uint32_t what = epoll_events_to_pop_what(events[i].events);
            pop_event_proc(event, what);
        }
    }
}

int pop_event_init(void)
{
    int epfd = epoll_create(MAX_EVENT_NUM);
    if (epfd < 0) {
        printf("Failed to create epoll fd, errno %d.", errno);
        return -1;
    }
    g_event_base.epfd = epfd;
    return 0;
}

void pop_event_fini(void)
{
    if (g_event_base.epfd != -1) {
        close(g_event_base.epfd);
        g_event_base.epfd = -1;
    }
}
