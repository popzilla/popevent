# popevent

#### 介绍
一个事件框架的轮子。

#### 使用说明

- 进入build目录下，执行`cmake ..`;
- 在build目录下，执行`make`即可生成目标文件`libpop_event.so`以及对应的测试程序。

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
