/*
 * Description: pop event测试代码
 * Author: xiasonglee@gmail.com
 * Date: 2023-3-4
 */

#define _GNU_SOURCE
#include "pop_event.h"

#include <fcntl.h>
#include <unistd.h>
#include <stdint.h>
#include <stdio.h>
#include <pthread.h>
#include <errno.h>
#include <string.h>

static void pop_event_proc(pop_event_t *event, uint32_t what, void *usrdata)
{
    int *pipefd = (int *)usrdata;
    if (what & POP_EVENT_IN) {
        char buf[32];
        read(pipefd[0], buf, sizeof(buf));
        printf("recv: %s\n", buf);
    }
}

static void *consumer_thread(void *arg)
{
    int *pipefd = (int *)arg;

    if (pop_event_init() != 0) {
        return NULL;
    }

    pop_event_t *event = pop_event_create(pipefd[0], pipefd);
    if (event == NULL) {
        pop_event_fini();
        return NULL;
    }

    uint32_t what = POP_EVENT_ET | POP_EVENT_IN;
    if (pop_event_add_watch(event, what, pop_event_proc) != 0) {
        pop_event_destroy(event);
        pop_event_fini();
        return NULL;
    }

    pop_event_loop();
    return NULL;
}

int main(void)
{
    int pipefd[2];
    if (pipe2(pipefd, O_NONBLOCK) != 0) {
        printf("Failed to create pipefd, errno %d.", errno);
        return -1;
    }

    pthread_t tid;
    if (pthread_create(&tid, NULL, consumer_thread, pipefd) != 0) {
        printf("Failed to create thread, errno %d.", errno);
        return -1;
    }

    for (int i = 0;; i++) {
        char buf[32];
        sprintf(buf, "hello, %d.", i);
        write(pipefd[1], buf, sizeof(buf));
        sleep(1); /* 等待1秒 */
    }
    return 0;
}
