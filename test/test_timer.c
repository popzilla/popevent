/*
 * Description: pop timer测试代码
 * Author: xiasonglee@gmail.com
 * Date: 2023-6-22
 */

#include <unistd.h>
#include <stdio.h>
#include <stdint.h>
#include "pop_event.h"
#include "pop_timer.h"

void timer_proc(pop_timer_t timer, void *usrdata)
{
    uint32_t *counter = (uint32_t *)usrdata;
    printf("counter: %u\n", *counter);
    (*counter)++;
}

int main(void)
{
    pop_event_init();

    static uint32_t counter = 0;
    pop_timer_create(500, true, timer_proc, &counter);

    pop_event_loop();
}
