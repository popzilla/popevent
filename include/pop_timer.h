/*
 * Description: pop timer定时器功能
 * Author: xiasonglee@gmail.com
 * Date: 2023-6-22
 */

#ifndef POP_TIMER_H
#define POP_TIMER_H

#include <stdint.h>
#include <stdbool.h>
#include "pop_event.h"

typedef void* pop_timer_t;

typedef void (*pop_timer_cb)(pop_timer_t timer, void *usrdata);

pop_timer_t pop_timer_create(uint32_t millsec, bool loop,
    pop_timer_cb cb, void *usrdata);

void pop_timer_destroy(pop_timer_t timer);

#endif
